import nProgress from 'nprogress';
import Vue from 'vue';
import VueRouter from 'vue-router';
// tslint:disable-next-line: no-default-import
import Home from './views/home.vue';
Vue.use(VueRouter);

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes: [
    {
      path: '/',
      name: 'home',
      component: Home,
    },
    {
      path: '/in',
      name: 'in',
      // Route level code-splitting
      // This generates a separate chunk (in.[hash].js) for this route
      // Which is lazy-loaded when the route is visited.
      component: () => import(/* webpackChunkName: "in" */ './views/in/in.vue'),
    },
    {
      path: '/in/project/:projectId/datasource/:datasourceId',
      name: 'in',
      // Route level code-splitting
      // This generates a separate chunk (in.[hash].js) for this route
      // Which is lazy-loaded when the route is visited.
      component: () => import(/* webpackChunkName: "in" */ './views/in/in.vue'),
    },
    {
      path: '/work/project/:projectId',
      name: 'work',
      component: () => import(/* webpackChunkName: "work" */ './views/work/work.vue'),
    },
    {
      path: '/out/project/:projectId',
      name: 'out',
      component: () => import(/* webpackChunkName: "out" */ './views/out/out.vue'),
    },
    {
      path: '/annotator/:documentId',
      name: 'annotator',
      component: () => import(/* webpackChunkName: "annotator" */ './components/annotator/annotator.vue'),
    },
  ],
});

router.beforeResolve((to, from, next) => {
  // If this isn't an initial page load.
  if (to.name) {
    // Start the route progress bar.
    // TODO: Top progressbar is not working. Spinner is working.
    // We need to do vice - a - versa.
    // Spinner is working. We need to do vice-a-versa. It is z-index issue.
    // Reported on their git repo.
    nProgress.start();
  }
  next();
});

router.afterEach((to, from) => {
  // Complete the animation of the route progress bar.
  nProgress.done();
});

export { router };
