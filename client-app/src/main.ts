import Axios, { AxiosResponse } from 'axios';
import locale from 'element-ui/lib/locale';
import lang from 'element-ui/lib/locale/lang/en';
import { StatusCodes } from 'http-status-codes';
import Vue from 'vue';
// tslint:disable-next-line: no-default-import
import App from './app.vue';
import './registerServiceWorker';
import { router } from './router';

// Configure language
locale.use(lang);

// Add a response interceptor
Axios.interceptors.response.use((response: AxiosResponse) => response, (error: any) => {

  // TODO: Use enum instead of 401
  if (error.response.status === StatusCodes.UNAUTHORIZED) {
    window.location = error.response.headers.location;
  }

  // Do something with response error
  return Promise.reject(error);
});

Vue.config.productionTip = false;

new Vue({
  router,
  render: (h) => h(App),
}).$mount('#app');
