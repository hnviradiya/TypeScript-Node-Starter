﻿import { Datasource } from './Datasource';

export interface DatasourceType {
    datasource: Datasource[];
    id: number;
    name: string;
}
