import { getModelForClass, prop } from '@typegoose/typegoose';
import { Document } from '../../api/document';
import { DatasourceType } from '../Enums/datasource-type';
import { ModelBase } from '../General/model-base';
import { Project } from '../Master/project';

export class Datasource extends ModelBase {
  @prop()
  authTokenId: number;
  @prop()
  documents: Document[];

  @prop()
  name: string;
  @prop()
  project: Project;
  @prop()
  source: number;
  @prop()
  type: DatasourceType;

  filter: string;

  constructor() {
    super();
    this.documents = [];
    this.project = new Project();
  }
}

const DatasourceModel = getModelForClass(Datasource);
export { DatasourceModel };
