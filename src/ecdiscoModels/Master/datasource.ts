import { getModelForClass, prop } from '@typegoose/typegoose';
import { DatasourceType } from '../Enums/datasource-type';
import { ModelBase } from '../General/model-base';

export class Datasource extends ModelBase {

  @prop()
  name: string;
  @prop()
  datasourceTypeId: number;
  @prop()
  datasourceType: DatasourceType;
}

const datasourceModel = getModelForClass(Datasource);
export { datasourceModel };
