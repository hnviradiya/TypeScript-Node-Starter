import { prop } from '@typegoose/typegoose';
import { ModelBase } from '../General/model-base';

export class DocumentMetadata extends ModelBase {

  @prop()
  name: string;
}
