import { getModelForClass, prop } from '@typegoose/typegoose';
import { ModelBase } from '../General/model-base';

export class DocumentMetadatum extends ModelBase {

  @prop()
  name: string;
}

const DocumentMetadatumModel = getModelForClass(DocumentMetadatum);
export { DocumentMetadatumModel };
