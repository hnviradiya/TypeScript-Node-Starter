﻿import { getModelForClass, prop, Ref } from '@typegoose/typegoose';
import { Condition } from '../Enums/Condition';
import { FieldType } from '../Enums/field-type';
import { NodeType } from '../Enums/node-type';
import { Operation } from '../Enums/Operation';
import { ModelBase } from './model-base';

export class QueryRule extends ModelBase {

  @prop()
  condition: Condition;
  @prop()
  rules: Ref<QueryRule | ChildRule>[];
  @prop()
  queryId: number;
  @prop()
  parentQueryRuleId: number;
}

export class ChildRule extends ModelBase {
  @prop()
  fieldId: number;
  @prop()
  fieldType: NodeType;
  @prop()
  type: FieldType;
  @prop()
  operation: Operation;
  @prop()
  value: string;
  @prop()
  parentQueryRuleId: number;
}

const QueryRuleModel = getModelForClass(QueryRule);
const ChildRuleModel = getModelForClass(ChildRule);
export { QueryRuleModel, ChildRuleModel };
