import { Module } from '@nestjs/common';
import { ServeStaticModule } from '@nestjs/serve-static';
import { join } from 'path';
import { ProjectController } from './api/Controllers/api/Master/Project/project-controller';
import { DatasourceController } from './api/Controllers/api/Project/Datasource/datasource-controller';
import { DocumentAnnotationController } from './api/Controllers/api/Project/Document/document-annotation-controller';
import { DocumentController } from './api/Controllers/api/Project/Document/document-controller';
import { DocumentSearchController } from './api/Controllers/api/Project/Document/document-search-controller';
import { IndexController } from './api/Controllers/api/Project/Document/index-controller';
import { DocumentFieldController } from './api/Controllers/api/Project/DocumentField/document-field-controller';
import { SessionController } from './api/Controllers/api/Project/General/session-controller';
import { ProductionController } from './api/Controllers/api/Project/Production/production-controller';
import { SearchController } from './api/Controllers/api/Project/Search/search-controller';
import { AuthRedirectController } from './api/Controllers/auth-redirect-controller';
import { HomeController } from './api/Controllers/home-controller';
import { UploadController } from './api/Controllers/upload-controller';
import { WebViewController } from './api/Controllers/web-view-controller';

@Module({
  imports: [
    ServeStaticModule.forRoot({ // New
      rootPath: join(__dirname, '..', 'client/dist'), // New
    }), // New
  ],
  controllers: [
    AuthRedirectController,
    HomeController,
    UploadController,
    WebViewController,
    ProjectController,
    DatasourceController,
    DocumentAnnotationController,
    DocumentController,
    DocumentSearchController,
    IndexController,
    DocumentFieldController,
    SessionController,
    ProductionController,
    SearchController,
  ],
})
export class AppModule {}
