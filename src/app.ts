import { INestApplication } from '@nestjs/common';
import { NestFactory } from '@nestjs/core';
import { NestExpressApplication } from '@nestjs/platform-express';
import { setGlobalOptions, Severity } from '@typegoose/typegoose';
import bluebird from 'bluebird';
import bodyParser from 'body-parser';
import compression from 'compression'; // compresses requests
import mongo from 'connect-mongo';
import cors from 'cors';
import express from 'express';
import flash from 'express-flash';
import session from 'express-session';
import lusca from 'lusca';
import mongoose from 'mongoose';
import passport from 'passport';
import path from 'path';
import { AppModule } from './app.module';
import { MONGODB_URI, SESSION_SECRET } from './util/secrets';

setGlobalOptions({ options: { allowMixed: Severity.ALLOW } });

const MongoStore = mongo(session);

// Create Express server
let app: INestApplication;

async function bootstrap() {
  app = await NestFactory.create<NestExpressApplication>(AppModule);
  app.setGlobalPrefix('api'); // New
  await app.listen(3000);
}

bootstrap();

// Connect to MongoDB
const mongoUrl = MONGODB_URI;
mongoose.Promise = bluebird;

mongoose
  .connect(mongoUrl, {
    useNewUrlParser: true,
    useCreateIndex: true,
    useUnifiedTopology: true,
  })
  .then(() => {
    /** ready to use. The `mongoose.connect()` promise resolves to undefined. */
  })
  .catch((err) => {
    console.log(
      `MongoDB connection error. Please make sure MongoDB is running. ${err}`
    );
    // process.exit();
  });

const corsOpts = {
  origin: 'http://localhost:8080',
} as cors.CorsOptions;

app.use(cors(corsOpts));

// Express configuration
// app.set('port', Number(process.env.PORT || 3000));
// app.set('views', path.join(__dirname, '../views'));
// app.set('view engine', 'pug');
app.use(compression());
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));
app.use(
  session({
    resave: true,
    saveUninitialized: true,
    secret: SESSION_SECRET,
    store: new MongoStore({
      url: mongoUrl,
      autoReconnect: true,
    }),
  })
);
app.use(passport.initialize());
app.use(passport.session());
app.use(flash());
app.use(lusca.xframe('SAMEORIGIN'));
app.use(lusca.xssProtection(true));
// app.use((req, res, next) => {
//   res.locals.user = req.user;
//   next();
// });
// app.use((req, res, next) => {
//   // After successful login, redirect back to the intended page
//   if (
//     !req.user &&
//     req.path !== '/login' &&
//     req.path !== '/signup' &&
//     !req.path.match(/^\/auth/) &&
//     !req.path.match(/\./)
//   ) {
//     req.session.returnTo = req.path;
//   } else if (req.user && req.path == '/account') {
//     req.session.returnTo = req.path;
//   }
//   next();
// });

app.use(
  express.static(path.join(__dirname, 'public'), { maxAge: 31557600000 })
);

/**
 * Primary app routes.
 */
// app.all('/', homeController.index);
// app.all('/login', userController.getLogin);
// app.all('/login', userController.postLogin);
// app.all('/logout', userController.logout);
// app.all('/forgot', userController.getForgot);
// app.all('/forgot', userController.postForgot);
// app.all('/reset/:token', userController.getReset);
// app.all('/reset/:token', userController.postReset);
// app.all('/signup', userController.getSignup);
// app.all('/signup', userController.postSignup);
// app.all('/contact', contactController.getContact);
// app.all('/contact', contactController.postContact);
// app.all('/account', passportConfig.isAuthenticated, userController.getAccount);
// app.all(
//   '/account/profile',
//   passportConfig.isAuthenticated,
//   userController.postUpdateProfile
// );
// app.all(
//   '/account/password',
//   passportConfig.isAuthenticated,
//   userController.postUpdatePassword
// );
// app.all(
//   '/account/delete',
//   passportConfig.isAuthenticated,
//   userController.postDeleteAccount
// );
// app.all(
//   '/account/unlink/:provider',
//   passportConfig.isAuthenticated,
//   userController.getOauthUnlink
// );

// /**
//  * API examples routes.
//  */
// app.all('/api/*', apiController.getApi);
// app.all(
//   '/api/facebook',
//   passportConfig.isAuthenticated,
//   passportConfig.isAuthorized,
//   apiController.getFacebook
// );

// /**
//  * OAuth authentication routes. (Sign in)
//  */
// app.all(
//   '/auth/facebook',
//   passport.authenticate('facebook', { scope: ['email', 'public_profile'] })
// );
// app.all(
//   '/auth/facebook/callback',
//   passport.authenticate('facebook', { failureRedirect: '/login' }),
//   (req, res) => {
//     res.redirect(req.session.returnTo || '/');
//   }
// );

export default app;
