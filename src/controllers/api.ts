'use strict';

import graph from 'fbgraph';
import { Response, Request, NextFunction } from 'express';
import { UserDocument } from '../models/User';
import requireDir from 'require-dir';
import { MasterController } from '../api/Controllers/api/Master/master-controller';
const apiDir = requireDir('../api', { recurse: true });

const api = 'api/';

/**
 * List of API examples.
 * @route GET /api
 */
export const getApi = (req: Request, res: Response): void => {
  const url = req.url;
  const urlWithoutApi = url.slice(url.indexOf(api) + api.length);
  const className = urlWithoutApi.split('/')[0];
  const methodName = urlWithoutApi.split('/')[1];
  let moduleObject: any;
  let classObject: any;

  for (const moduleName of Object.getOwnPropertyNames(apiDir)) {
    if (moduleName.toUpperCase() == className.toUpperCase()) {
      moduleObject = apiDir[moduleName];

      for (const className of Object.getOwnPropertyNames(moduleObject)) {
        if (moduleName.toUpperCase() == className.toUpperCase()) {
          classObject = moduleObject[className];

          if (req.header('projectId')) {
            // TODO: Here only if permission is there then only need to set.
            classObject.projectId = req.header('projectId');
          }

          for (const methodName of Object.getOwnPropertyNames(classObject)) {
            if (methodName.toUpperCase() == methodName.toUpperCase()) {
              const masterController = classObject[
                methodName
              ] as MasterController;
              masterController.request = req;
              masterController.response = res;
              break;
            }
          }

          break;
        }
      }

      break;
    }
  }

  res.json(apiDir['document']['Document']['getDocuments']());
};

/**
 * Facebook API example.
 * @route GET /api/facebook
 */
export const getFacebook = (
  req: Request,
  res: Response,
  next: NextFunction
): void => {
  const user = req.user as UserDocument;
  const token = user.tokens.find((token) => token.kind === 'facebook');
  graph.setAccessToken(token.accessToken);
  graph.get(
    `${user.facebook}?fields=id,name,email,first_name,last_name,gender,link,locale,timezone`,
    (err: Error, results: graph.FacebookUser) => {
      if (err) {
        return next(err);
      }
      res.render('api/facebook', {
        title: 'Facebook API',
        profile: results,
      });
    }
  );
};
