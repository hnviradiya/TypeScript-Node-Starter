export class ChunkMetaData {
  UploadUid: string;
  FileName: string;
  RelativePath: string;
  ContentType: string;
  ChunkIndex: number;
  TotalChunks: number;
  TotalFileSize: number;
}
