import { Paginate } from '../../../../../ecdiscoModels/General/Paginate';

export class GridData {
  documents: any[];
  paginate: Paginate;
}
